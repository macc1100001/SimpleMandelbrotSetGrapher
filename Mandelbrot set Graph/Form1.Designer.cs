﻿namespace Mandelbrot_set_Graph
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.moveXTrackBar = new System.Windows.Forms.TrackBar();
            this.moveYTrackBar = new System.Windows.Forms.TrackBar();
            this.scrollXLabel = new System.Windows.Forms.Label();
            this.scrollYLabel = new System.Windows.Forms.Label();
            this.zoomTrackBar = new System.Windows.Forms.TrackBar();
            this.zoomLabel = new System.Windows.Forms.Label();
            this.maxIterTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.applyChangesButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveXTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveYTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 400);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // moveXTrackBar
            // 
            this.moveXTrackBar.Location = new System.Drawing.Point(53, 430);
            this.moveXTrackBar.Name = "moveXTrackBar";
            this.moveXTrackBar.Size = new System.Drawing.Size(290, 45);
            this.moveXTrackBar.TabIndex = 1;
            this.moveXTrackBar.TabStop = false;
            this.moveXTrackBar.Scroll += new System.EventHandler(this.moveXTrackBar_Scroll);
            // 
            // moveYTrackBar
            // 
            this.moveYTrackBar.Location = new System.Drawing.Point(434, 79);
            this.moveYTrackBar.Name = "moveYTrackBar";
            this.moveYTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.moveYTrackBar.Size = new System.Drawing.Size(45, 290);
            this.moveYTrackBar.TabIndex = 2;
            this.moveYTrackBar.TabStop = false;
            this.moveYTrackBar.Scroll += new System.EventHandler(this.moveYTrackBar_Scroll);
            // 
            // scrollXLabel
            // 
            this.scrollXLabel.AutoSize = true;
            this.scrollXLabel.Location = new System.Drawing.Point(184, 478);
            this.scrollXLabel.Name = "scrollXLabel";
            this.scrollXLabel.Size = new System.Drawing.Size(35, 13);
            this.scrollXLabel.TabIndex = 3;
            this.scrollXLabel.Text = "label1";
            // 
            // scrollYLabel
            // 
            this.scrollYLabel.AutoSize = true;
            this.scrollYLabel.Location = new System.Drawing.Point(431, 372);
            this.scrollYLabel.Name = "scrollYLabel";
            this.scrollYLabel.Size = new System.Drawing.Size(35, 13);
            this.scrollYLabel.TabIndex = 4;
            this.scrollYLabel.Text = "label2";
            // 
            // zoomTrackBar
            // 
            this.zoomTrackBar.Location = new System.Drawing.Point(434, 459);
            this.zoomTrackBar.Name = "zoomTrackBar";
            this.zoomTrackBar.Size = new System.Drawing.Size(255, 45);
            this.zoomTrackBar.TabIndex = 5;
            this.zoomTrackBar.TabStop = false;
            this.zoomTrackBar.Scroll += new System.EventHandler(this.zoomTrackBar_Scroll);
            // 
            // zoomLabel
            // 
            this.zoomLabel.AutoSize = true;
            this.zoomLabel.Location = new System.Drawing.Point(540, 507);
            this.zoomLabel.Name = "zoomLabel";
            this.zoomLabel.Size = new System.Drawing.Size(34, 13);
            this.zoomLabel.TabIndex = 6;
            this.zoomLabel.Text = "Zoom";
            // 
            // maxIterTextBox
            // 
            this.maxIterTextBox.Location = new System.Drawing.Point(595, 51);
            this.maxIterTextBox.Name = "maxIterTextBox";
            this.maxIterTextBox.Size = new System.Drawing.Size(100, 20);
            this.maxIterTextBox.TabIndex = 7;
            this.maxIterTextBox.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(471, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Maximum # of iterations";
            // 
            // applyChangesButton
            // 
            this.applyChangesButton.AutoSize = true;
            this.applyChangesButton.Location = new System.Drawing.Point(553, 140);
            this.applyChangesButton.Name = "applyChangesButton";
            this.applyChangesButton.Size = new System.Drawing.Size(87, 23);
            this.applyChangesButton.TabIndex = 9;
            this.applyChangesButton.TabStop = false;
            this.applyChangesButton.Text = "Apply changes";
            this.applyChangesButton.UseVisualStyleBackColor = true;
            this.applyChangesButton.Click += new System.EventHandler(this.applyChangesButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 561);
            this.Controls.Add(this.applyChangesButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.maxIterTextBox);
            this.Controls.Add(this.zoomLabel);
            this.Controls.Add(this.zoomTrackBar);
            this.Controls.Add(this.scrollYLabel);
            this.Controls.Add(this.scrollXLabel);
            this.Controls.Add(this.moveYTrackBar);
            this.Controls.Add(this.moveXTrackBar);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mandelbrot set";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveXTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moveYTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TrackBar moveXTrackBar;
        private System.Windows.Forms.TrackBar moveYTrackBar;
        private System.Windows.Forms.Label scrollXLabel;
        private System.Windows.Forms.Label scrollYLabel;
        private System.Windows.Forms.TrackBar zoomTrackBar;
        private System.Windows.Forms.Label zoomLabel;
        private System.Windows.Forms.TextBox maxIterTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button applyChangesButton;
    }
}


﻿//#define DEBUG
#undef DEBUG

using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Mandelbrot_set_Graph
{
    public partial class Form1 : Form
    {
        private Graphics[] g;
        private Graphics buffer, mainFrame;
        private Bitmap bm;
        private CoOrds[] graphPoints;

        private const double valueX = -1.5, valueY = 2.0;   //Original values are x = -1.0; y = 2.0

        private double initialX = valueX, initialY = valueY; 
        private double newInitX, newInitY, zoomFactor;
        private int maxIterations = 100;
        private int maxDistance = 10000;


        private struct CoOrds
        {
            public float x, y;
            public int iterationsNeeded;
            public double distance;

            public CoOrds(float coordX, float coordY, int iterations, double r)
            {
                x = coordX;
                y = coordY;
                iterationsNeeded = iterations;
                distance = r;
            }
        }

        public Form1()
        {
            InitializeComponent();
            moveXTrackBar.Minimum =(int) ((valueX - 0.5) * 1000);
            moveXTrackBar.Maximum = (int)-((valueX - 0.5) * 1000);
            moveYTrackBar.Minimum = (int)-valueY * 1000;
            moveYTrackBar.Maximum = (int)valueY * 1000;
            zoomTrackBar.Minimum = 1;
            zoomTrackBar.Maximum = 2000;

            moveXTrackBar.Value = (int)(initialX * 1000);
            moveYTrackBar.Value = (int)(initialY * 1000);
            zoomTrackBar.Value = 1000;

            zoomFactor = zoomTrackBar.Value / 1000.0;
            newInitX = moveXTrackBar.Value / 1000.0;
            newInitY = moveYTrackBar.Value / 1000.0;

            scrollXLabel.Text = String.Format("X axis at: {0}", moveXTrackBar.Value / 1000.0);
            scrollYLabel.Text = String.Format("Y axis at: {0}", moveYTrackBar.Value / 1000.0);
            zoomLabel.Text = String.Format("Zoom factor: {0}", zoomFactor);

            graphPoints = new CoOrds[pictureBox1.Width * pictureBox1.Height];
            g = new Graphics[pictureBox1.Width * pictureBox1.Height];
        }

        private async void pictureBox1_Click(object sender, EventArgs e)
        {
#if (DEBUG)
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
#endif
            zoomTrackBar.Enabled = false;
            moveXTrackBar.Enabled = false;
            moveYTrackBar.Enabled = false;
            maxIterTextBox.Enabled = false;
            applyChangesButton.Enabled = false;

            mainFrame = pictureBox1.CreateGraphics();
            bm = new Bitmap(400, 400);
            buffer = Graphics.FromImage(bm);

            Task workingTask = new Task(DoConsumingWork);
            workingTask.Start();
            await workingTask;
            Task plottingTask = new Task(() =>
            {
                if (workingTask.IsCompleted)
                {
                    for(int i = 0; i < graphPoints.Length; i++)
                    {
                        Graph(i);
                    }
                    //mainFrame.DrawImage(bm, 0, 0);
                    pictureBox1.Image = bm;
                }
                //MessageBox.Show("Done");
            });
            pictureBox1.Refresh();

            plottingTask.Start();

            //MessageBox.Show("Application status: " + plottingTask.Status.ToString());
            await plottingTask;
            zoomTrackBar.Enabled = true;
            moveXTrackBar.Enabled = true;
            moveYTrackBar.Enabled = true;
            maxIterTextBox.Enabled = true;
            applyChangesButton.Enabled = true;


#if (DEBUG)
            //Debug code to view execution time
            stopwatch.Stop();
            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.TotalMilliseconds);
            MessageBox.Show("RunTime " + elapsedTime);

            //paper.DrawLine(new Pen(Color.Blue), (pictureBox1.Width / 2), 0, (pictureBox1.Width / 2), pictureBox1.Height);  //draws the y axis
            //paper.DrawLine(new Pen(Color.Violet), 0, (pictureBox1.Height / 2), pictureBox1.Width, (pictureBox1.Height / 2)); // draws the x axis
#endif
        }

        private void moveXTrackBar_Scroll(object sender, EventArgs e)
        {
            newInitX = moveXTrackBar.Value / 1000.0;
            scrollXLabel.Text = String.Format("X axis at: {0}", moveXTrackBar.Value / 1000.0);
        }

        private void moveYTrackBar_Scroll(object sender, EventArgs e)
        {
            newInitY = moveYTrackBar.Value / 1000.0;
            scrollYLabel.Text = String.Format("Y axis at: {0}", moveYTrackBar.Value / 1000.0);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if(bm != null)
            {
                mainFrame.DrawImage(bm, 0, 0);
            }
        }

        private void zoomTrackBar_Scroll(object sender, EventArgs e)
        {
            zoomFactor = zoomTrackBar.Value / 1000.0;
            zoomLabel.Text = String.Format("Zoom factor: {0}", zoomFactor);
        }

        private void applyChangesButton_Click(object sender, EventArgs e)
        {
            try
            {
                maxIterations = Convert.ToInt32(maxIterTextBox.Text);
                if (maxIterations < 100)
                {
                    maxIterations = 100;
                    MessageBox.Show("Iterations must be atleast 100! To ensure atleast something will be drawn, iterations were automatically reseted to 100", "Error");
                    maxIterTextBox.Text = "";
                }
                else
                {
                    MessageBox.Show("Applied changes");
                    maxIterTextBox.Text = "";
                }
            }
            catch (FormatException)
            {
                if (maxIterTextBox.Text == "")
                {
                    MessageBox.Show("Please introduce a value", "Error");
                    maxIterTextBox.Text = "";
                }
                else
                {
                    MessageBox.Show("Please introduce numbers only!", "Error");
                    maxIterTextBox.Text = "";
                }
            }
        }

        private void DoConsumingWork()
        {
            double factor = zoomFactor; //modify this to zoom in and zoom out
            int realX = 0, realY = 0;
            int index = 0;

            initialX = newInitX;
            initialY = newInitY;

            do
            {
                ComputeValues(realX, realY, index);
                index++;
                initialX += factor * 0.01;
                realX++;

                if (realX == pictureBox1.Width)
                {
                    initialX = newInitX;
                    realX = 0;

                    initialY -= factor * 0.01;
                    realY++;
                }
            }
            while (realY < pictureBox1.Width);

            initialX = newInitX;
            initialY = newInitY;

        }

        private void ComputeValues(int coordX, int coordY, int index)
        {
            int iterations = 0;
            double r = 0;

            double newX, newY, prevX, prevY;
            prevX = initialX;
            prevY = initialY;

            for (int i = 1; i <= maxIterations && r < maxDistance; i++)
            {
                newX = (prevX * prevX) - (prevY * prevY) - initialX;
                newY = 2 * prevX * prevY - initialY;

                prevX = newX;
                prevY = newY;

                r = Math.Sqrt((newX * newX) + (newY * newY));
                iterations++;
            }
            if(index < graphPoints.Length)
            {
                graphPoints[index] = new CoOrds(coordX, coordY, iterations, r);
            }
        }
        private void Graph(int index)
        {
            if(graphPoints[index].distance < maxDistance)
            {
                buffer.FillRectangle(new SolidBrush(Color.Black), graphPoints[index].x, graphPoints[index].y, 1, 1);
            }
            else
            {
                if (graphPoints[index].iterationsNeeded <= 4)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#FF0000")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 6)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#CC0033")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 8)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#BF0040")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 10)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#B2004C")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 12)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#A60059")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 14)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#990066")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 16)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#8C0073")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 18)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#800080")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 20)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#73008C")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 24)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#660099")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 26)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#5900A6")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 28)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#4D00B2")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 30)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#4000BF")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 32)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#3300CC")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 34)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#2600D9")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 38)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#0026AD")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 40)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#0033A3")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 42)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#004099")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 44)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#004C8F")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 46)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#005985")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 48)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#00667A")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 50)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#007370")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 52)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#3333FF")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 54)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#3B30FC")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 56)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#422EFA")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 58)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#4A2BF7")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 60)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#5229F5")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else if (graphPoints[index].iterationsNeeded <= 62)
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#5926F2")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
                else
                {
                    buffer.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#CC00CC")), graphPoints[index].x, graphPoints[index].y, 1, 1);
                }
            }
        }
    }
}
# Graficador simple del conjunto de Mandelbrot

## TODO

- [ ] Agregar dibujado por gpu para acelerar el tiempo de dibujado y poder utilizar una resolucion mayor
- [x] Cambiar la paleta de colores por una mas atractiva visualmente

### "Known issues"
- Mover los scrollers de las "trackBars" y hacer click sobre el pictureBox, no siempre redibuja la nueva posicion con el primer click
- La seccion que colorea las regiones, junto con los colores esta "Hard-codeada"
